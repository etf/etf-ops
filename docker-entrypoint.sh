#!/bin/bash

source /usr/bin/etf-init.sh

cat << "EOF"
 _________
< ETF OPS >
 ---------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
EOF

print_header

etf_update

start_xinetd

etf_init

copy_certs

apache_init

config_web_access

config_alerts

init_api

config_htcondor

config_etf

fix_cmk_theme

etf_start

echo "Fetching Ops credentials ..."
su etf -c "/usr/lib/nagios/plugins/globus/refresh_proxy --myproxyuser nagios -H myproxy.cern.ch -t 120 --key /opt/omd/sites/etf/etc/nagios/globus/etf_srv_key.pem --vo ops --lifetime 24 --name NagiosRetrieve-ETF-ops -x /opt/omd/sites/etf/etc/nagios/globus/userproxy.pem --cert /opt/omd/sites/etf/etc/nagios/globus/etf_srv_cert.pem"

etf_wait
