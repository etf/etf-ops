export GLITE_LOCATION="/usr"
export GLOBUS_TCP_PORT_RANGE="20000,25000"
export GLITE_LOCATION_VAR="/var"
export GLOBUS_HOSTNAME="`hostname`"
export MYPROXY_SERVER="myproxy.cern.ch"
export LCG_LOCATION="/usr"
export GRID_ENV_LOCATION="/usr/libexec"
export LCG_GFAL_INFOSYS="top-bdii.cern.ch:2170"
