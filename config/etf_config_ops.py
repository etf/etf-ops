import logging

from ncgx.inventory import Hosts, Checks

log = logging.getLogger('ncgx')

HTCONDOR_METRICS = (
    'org.sam.CONDOR-JobSubmit-ops',
)

SERVICES = [
    ("ce503.cern.ch", "HTCONDOR-CE", "ce503.cern.ch:9619"),
    ("ce513.cern.ch", "HTCONDOR-CE", "ce513.cern.ch:9619")
]

def run():
    # Hardcode for the moment some CEs in CERN-PROD
    # list of tuples (hostname, flavor, endpoint)
    services = SERVICES

    # Add hosts, each tagged with corresponding flavors
    # creates /etc/ncgx/conf.d/generated_hosts.cfg
    h = Hosts()
    for service in services:
        h.add(service[0], tags=[service[1]])
    h.serialize()

    # Add corresponding metrics to tags
    # creates /etc/ncgx/conf.d/generated_checks.cfg
    c = Checks()
    c.add_all(HTCONDOR_METRICS, tags=["HTCONDOR-CE", ])

    # Queues
    for service in services:
        host = service[0]
        c.add('org.sam.CONDOR-JobState-ops', hosts=(host,),
              params={'args': {'--resource': 'condor://%s' % host}})

    c.serialize()
